FROM openjdk:8-jdk-alpine

WORKDIR /app/demo

COPY pom.xml .
COPY .mvn/ .mvn
COPY mvnw .
COPY Dockerfile .

#saltar el test y compilacion no compilar ni ejecutar el codigo fuente empaquetar proeyecto sin codigo fuente
RUN ./mvnw clean package -Dmaven.test.skip -Dmaven.main.skip -Dspring-boot.repackage.skip && rm -r ./target/

COPY src ./src


RUN ./mvnw clean package -DskipTests


EXPOSE 9091

ENTRYPOINT ["java","-jar","./target/demo-0.0.1-SNAPSHOT.jar"]