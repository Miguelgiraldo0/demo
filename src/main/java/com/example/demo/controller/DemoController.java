package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/hello")
    public String hello() {
        return "La historia de la creación del universo es un tema que ha sido estudiado y debatido por científicos, religiosos y filósofos desde tiempos antiguos. Según la teoría científica más aceptada, el Big Bang, el universo se originó hace aproximadamente 13.800 millones de años a partir de una gran explosión que lanzó materia y energía en todas las direcciones. Durante los primeros segundos después del Big Bang, el universo estaba extremadamente caliente y denso, pero se expandió y enfrió rápidamente. Con el tiempo, la materia se agrupó formando estrellas y planetas, y eventualmente, la vida apareció en la Tierra.\n" +
                "\n" +
                "Sin embargo, existen diferentes creencias y teorías sobre la creación del universo. Algunas religiones tienen sus propias narraciones de la creación, como el Génesis en el cristianismo, el Rigveda en el hinduismo y el Alá en el Islam.\n" +
                "\n" +
                "En resumen, la historia de la creación del universo es un tema complejo y multidisciplinario, en el que tanto la ciencia como las creencias religiosas y las teorías filosóficas han tratado de dar una explicación coherente y comprensible del origen del universo.";
    }
}
